# Comunidades de Software Livre e HackerSpace no Brasil

O mundo é movido por tecnologia, sofware e conhecimento livre! Participe você também e vamos conversar. Esse repositório lista as comunidades (e seus meios de comunicação) relacionados a sofware livre e hackerspace no Brasil. Use também para divulgar esse grupos e canais fora do Telegram. Siga nosso canal no Telegram: [Comunidades de Software Livre e HackerSpace no Brasil](https://t.me/SoftwareLivreHackerSpaceBrasil)! Avante!

Inspiração no [Comunidades Potiguares de TI](https://gitlab.com/pbaesse/comunidades-ti-rn). Conheça várias outras comunidades lá também.

## Sempre em construção. Favorite e contribua aqui no repositório com sua edição!

Para realizar pedidos de novas comunidades:

1.  Crie um pull request para adicionar novas comunidades;
2.  Retire o :triangular\_flag\_on\_post: das comunidades com esse ícone e coloque nas novas comunidades adiciondas;
3.  Avise no grupo do Telegram do [PotiLivre](http://t.me/potilivre) ou do [BrejaTec](http://t.me/brejatec) sobre a alteração para que seja atualizado também no canal do Telegram.  
    **OBS**: Caso não saiba usar o Git, basta pedir para que sua comunidade seja adicionada aqui no grupo do telegram do [BrejaTec](http://t.me/brejatec) ou do [PotiLivre](http://t.me/potilivre).

# Software Livre

[PotiLivre - Comunidade Potiguar de Software Livre](http://t.me/PotiLivre) :triangular\_flag\_on\_post: 

[Comunidade Software Livre - Ponto de encontro de Comunidades Software Livre](http://t.me/ComunidadeSoftwareLivre) :triangular\_flag\_on\_post:  

[LibreCodeCoop - Cooperativa de Software Livre](http://t.me/LibreCodeCoop) :triangular\_flag\_on\_post:  

[Latinoware - Congresso Latino-americano de Software Livre](http://t.me/Latinoware) :triangular\_flag\_on\_post:  

[Movimento Software Livre - Canal do Movimento Software Livre Brasil](http://t.me/MovimentoSoftwareLivre) :triangular\_flag\_on\_post:  

[ParaLivre - Comunidade Paraense de Software Livre](http://t.me/ParaLivre) :triangular\_flag\_on\_post:  

[JampaLivre - Comunidade Paraibana de Software Livre](http://t.me/JampaLivre) :triangular\_flag\_on\_post:  

[GovBR - Software Livre no Setor Público](http://t.me/GovBR) :triangular\_flag\_on\_post:  

[Áudio e Software Livre Lusofono](http://t.me/AudioeSoftwareLivreLusofono) :triangular\_flag\_on\_post: 

[Software Livre em Curitiba](http://t.me/SoftwareLivre_Curitiba) :triangular\_flag\_on\_post:  

[Canal Software Livre Popular](http://t.me/SoftwareLivrePopular) :triangular\_flag\_on\_post:  

[Software Livre Popular - Grupo anticapitalista, focado em discussões acerca de cultura livre no âmbito da tecnologia](http://t.me/SoftwareLivrePopularg) :triangular\_flag\_on\_post:

# HackerSpace

[HackerSpacesBr - Grupo de hackerspaces do Brasil](http://t.me/HackerSpacesBr) :triangular\_flag\_on\_post: 

[MateHackersPoa - HackerSpace de Porto Alegre](http://t.me/MateHackersPoa) :triangular\_flag\_on\_post: